FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["src/AzureTest/AzureTest.csproj", "AzureTest/"]

RUN dotnet restore "AzureTest/AzureTest.csproj"
COPY src/ .
WORKDIR "/src/AzureTest"
RUN dotnet build "AzureTest.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "AzureTest.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AzureTest.dll"]

###